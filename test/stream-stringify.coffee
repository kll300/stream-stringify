sstringify = require('../src/stream-stringify')

tests =
        
    stringifyNestedJson :  (test) ->
        
        test.expect(13)
        
        
        
        
        simpleInput = [2]
        
        
        
        complexInput = {
            outerAttr: 1,
            outerOtherAttr: 2,
            innerObj: {
                    nestedAttr: [3,"4",5],
                    nestedSingleAttr: [{singleKey:6}],
                    nestedArray: [
                        7,8,9,[
                            10,11,12,{
                                innermostAttribute: 13
                            }
                        ]
                    ]
                }
            }
        
        inputWithEmpty = {
            first: {
                inner: 1,
                secondInner: "value",
                thirdInner: 3
                },
            second : [{},{2: 3, innerinner: [{},2]},[]],
            third: "Hello world"
        }

        inputWithoutEmpty = {
            first: {
                inner: 1,
                secondInner: "value",
                thirdInner: 3
                },
            second : [{2: 3, innerinner: [2]}],
            third: "Hello world"
        }
        
        
        expectedNoEmpty = JSON.stringify(inputWithoutEmpty)
        expectedSimple = JSON.stringify(simpleInput)
        expectedNormal = JSON.stringify(complexInput)
        expectedPretty = JSON.stringify(complexInput,null,2)
        expectedPrettySpacey = JSON.stringify(complexInput,null,5)
        expectedWithEmpty = JSON.stringify(inputWithEmpty,null,2)
        
    
        
        fillRecursive = (currentObject,currentStream) ->
            if Array.isArray(currentObject)
                for element in currentObject
                    if Array.isArray element
                        fillRecursive element,(currentStream.startNewArray())
                    else if typeof element == 'object'
                        
                        #Object (non-array)
                        fillRecursive element, (currentStream.startNewObject())
                    else
                        #number
                        currentStream.writeElement(element)
                currentStream.stop()
                
            else
                #Object (non-array)
                for key,value of currentObject
                    if Array.isArray value
                        newStream = currentStream.startNewArray(key)
                        fillRecursive value,newStream
                    else if typeof value == 'object'
                        #Object (non-array)
                        newStream = currentStream.startNewObject(key)
                        fillRecursive value,newStream
                    else
                        #number
                        currentStream.writeKeyValue(key,value)
                currentStream.stop()
                
                

        resultSimple = ''
        resultNormal = ''
        resultPausedNormal = ''
        resultPretty = ''
        resultPrettySpacey = ''
        resultToConvert = ''
        resultEmpty = ''
        resultEmptyArray = ''
        resultEmptyObject = ''
        resultWithEmpty = ''
        resultNoEmpty = ''
        resultNoStop = ''
        
        @sstringifySimple = sstringify({type : "Array", prettyFormat: false}).on('data',(data) -> resultSimple += data)
        @sstringifyWithEmpty = sstringify({type : "Object", prettyFormat: true, debug : true}).on('data',(data) -> resultWithEmpty += data)
        sstringifyNoStop = sstringify({ prettyFormat: false}).on('data',(data) -> resultNoStop += data)
        @sstringifyNormal = sstringify({prettyFormat : false}).on('data',(data) -> resultNormal += data)
        @sstringifyNoEmpty = sstringify({prettyFormat : false,outputEmpty : false}).on('data',(data) -> resultNoEmpty += data)
        @sstringifyPrettySpacey = sstringify({prettyFormat : true, tabWidth:5}).on('data',(data) -> resultPrettySpacey += data)
        
        @sstringifyPausedNormal =  sstringify({prettyFormat : false}).pause()
        @sstringifyPausedPretty =  sstringify({prettyFormat : true}).pause()
        
        @sstringifyToConvert = sstringify({prettyFormat : true, type : "Array"}).on('data',(data) -> resultToConvert += data)
        
        @sstringifyEmptyArray = sstringify({prettyFormat : true, type : "Array"}).on('data',(data) -> resultEmptyArray += data)
        @sstringifyEmptyArray.stop()
        
        @sstringifyEmptyObject = sstringify({prettyFormat : true, type : "Object"}).on('data',(data) -> resultEmptyObject += data)
        @sstringifyEmptyObject.stop()
        
        @sstringifyEmpty = sstringify({prettyFormat : true, outputEmpty : false, type: "Array"}).on('data',(data) ->
                                                                                                    console.log data
                                                                                                    resultEmpty += data)
        
        
        @sstringifyPausedPretty.on('data',(data) ->
            resultPretty += data)
        
        @sstringifyPausedNormal.on('data',(data) -> resultPausedNormal += data)
        
        
        arrayWithoutStop = sstringifyNoStop.startNewArray("newOne")
        arrayWithoutStop.writeElement(1)
        arrayWithoutStop.writeElement(2)
        arrayWithoutStop.stop()
        
        interruptingArray = sstringifyNoStop.startNewArray("anotherOne")
        interruptingArray.writeElement(3)
        interruptingArray.writeElement(4)
        interruptingArray.stop()
            
        
        sstringifyNoStop.stop()
        
        
        
        @sstringifyToConvert.writeElement 1
        @sstringifyToConvert.writeElement "Second"
        @sstringifyToConvert.writeElement [1,2,34]
        @sstringifyToConvert.writeElement [3]
        @sstringifyToConvert.writeElement {a:2,b:4}
        
    
        @sstringifyToConvert.stop()
        
        #Add loads of empty things
        for i in [1..14]
            @sstringifyAlsoEmpty = @sstringifyEmpty.startNewObject()
            #@sstringifyAlsoEmpty.stop()
        
        for i in [1..14]
            @sstringifyAlsoEmpty = @sstringifyEmpty.startNewArray()
            #@sstringifyAlsoEmpty.stop()
        
        
        
        @sstringifyEmpty.stop()
        
        
        expectedConverted = JSON.stringify(JSON.parse(resultToConvert),null,2)
        
        

        fillRecursive(simpleInput,@sstringifySimple)
        fillRecursive(complexInput,@sstringifyPausedNormal)
        fillRecursive(complexInput,@sstringifyPausedPretty)
        fillRecursive(complexInput,@sstringifyPrettySpacey)
        fillRecursive(inputWithEmpty,@sstringifyNoEmpty)
        fillRecursive(complexInput,@sstringifyNormal)
        fillRecursive(inputWithEmpty,@sstringifyWithEmpty)
        
        console.log resultToConvert
        console.log expectedConverted
        
        test.ok(expectedConverted == resultToConvert,"Correctly parsing input in other forms than strings")
        test.ok(resultNoStop == '{"newOne":[1,2],"anotherOne":[3,4]}',"Calling stop() automatically")
        test.ok(resultEmptyArray == "[]","Correctly parsing empty array")
        test.ok(resultEmptyObject == "{}","Correctly parsing empty object")
        test.ok(resultEmpty == "","Correctly parsing empty object with options not to stream it")
        test.ok(resultSimple == expectedSimple,"JSON Stringify produces same results as stream-stringify (simple)")
        test.ok(expectedNoEmpty == resultNoEmpty,"JSON Stringify produces same results as stream-stringify (testing with empty object)")
        
        
        
        
        
        test.ok(expectedWithEmpty == resultWithEmpty,"Successfully processing input with empty structures")
        test.ok(resultNormal == expectedNormal,"JSON Stringify produces same results as stream-stringify (complex)")
        test.ok(resultPrettySpacey == expectedPrettySpacey,"JSON Stringify produces same results as stream-stringify pretty print with spacey configuation")
        test.ok(sstringify.activeStreams == 0, "Everything successfully cleaned up")
        test.ok(resultPausedNormal==expectedNormal,"JSON.Stringify produces same result as stream-stringify (paused)")
        test.ok(resultPretty==expectedPretty,"JSON.Stringify pretty produces same result as stream-stringify with pretty options (paused)")
        test.done()
        
        

        
module.exports = tests
