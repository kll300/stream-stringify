Stream = require 'stream'
_ = require 'underscore'



#Heavily inspired by "through", https://www.npmjs.com/package/through
class StreamStringify  extends Stream.Stream
    
    #Good to know in order to prevent memory leaks
    @noStreams = 0
    
    #Save options to this in order to reuse it to other child streams
    constructor : (@options = {}) ->
        #Begin code from through
        @ended =  @_ended = @destroyed = @stopped = @hasNested = @isNested = false
        @buffer = ''
        
        
        #Init the base class stream
        super
        
        #Attributes important for the stream class
        @readable = @writable = @first = true
        @paused = false
        
        {@autoDestroy, @prettyFormat, @tabLevel, outputTokens, @tabWidth, @type, @outputEmpty, @preData, @isNested, @debug} = @options ? {}
        @autoDestroy ?= true
        @prettyFormat ?= true
        @outputEmpty ?= true
        @type ?= "Object"
        @isNested ?= false
        
        StreamStringify.noStreams += 1
        
        defaultOutputFormat = {op: "{",sep: ",",cl: "}"}
        
        
        
        switch @type
            when "Array"
                [@op,@sep,@cl] = ["[",",","]"]
            when "Object"
                {@op,@sep,@cl} = defaultOutputFormat
            else
                {@op,@sep,@cl} = _.extend(defaultOutputFormat,@options.outputTokens ? {})
        
        
        @preData = (@preData ? "") + @op
                
            

        #If there's going to be pretty formatting
        if @prettyFormat
            @tabLevel ?= 1
            @tabWidth ?= 2
            @tabCharacter = StreamStringify._tabCharacter(@tabWidth*@tabLevel)
            @prevTabCharacter = StreamStringify._tabCharacter(@tabWidth*(@tabLevel-1))
        
        
        @on('end', ->
            @readable = false
            if @autoDestroy
                process.nextTick( =>
                    @destroy()))
        
        
    
    #Formatting functions
    _keyPrefix : (key) -> "\"#{key}\":" + (if @prettyFormat then " " else "")
    
    #Produces a tab character with specified width of spaces
    @_tabCharacter : (width) ->
        if width
            (" " for k in [1..(width)]).join('')
        else
            ''
        

    #Applies citation if needed, or JSON.stringify if necessary
    _formatEntity: (data) ->
        switch
            when typeof data == "string" and data.length
                "\"#{data}\""
            when typeof data == 'object'
                if @prettyFormat
                    #Here we need to add the tab level that we already are at
                    data = JSON.stringify data,2,@tabWidth
                    dataLines = data.split('\n')
                    if dataLines.length > 2
                        for i in [1..dataLines.length-1]
                            dataLines[i] = @tabCharacter + dataLines[i]
                    data = dataLines.join('\n')
                    return data
                else
                    return JSON.stringify data
            else
                data
    
    #Applies a separator or operator
    _applySeparator : (data) ->
        
        if @first
            #@first = false
            data = @preData + (if @prettyFormat then "\n#{@tabCharacter}" else "") + data
            
            return data
        else
            @sep + (if @prettyFormat then "\n#{@tabCharacter}" else "") + data


    #Writes a key and a value. Meant to be used by objects
    writeKeyValue : (key,value) ->
        if @type != "Object"
            console.warn "Error: Cannot call writeKeyValue when initialized with type='Array', use 'Object' instead."
            return false
        @write @_applySeparator "#{@_keyPrefix key}#{@_formatEntity value}"
    
    #Writes an element. Meant to be used by arrays
    writeElement : (data)  ->
        if @type != "Array"
            console.warn "Error: Cannot call writeElement when initialized with type='Object', use 'Array' instead."
            return false
        @write @_applySeparator @_formatEntity data
    
    write : (data) ->
        
        #Empty predata, it has to have been outputted to the buffer
        @first = false
        @preData = ''
        @queue data
        return not @paused
    
    _specifyNestPrefix : (key) ->
        if key?
            if @type == "Array"
                console.warn "Error: Cannot nest with specified key when initialized with type='Object', use 'Array' instead"
                return false
            @initialData = @_applySeparator @_keyPrefix key
        else
            if @type == "Object"
                console.warn "Error: Cannot nest missing first argument when initialized with type='Array'. Use type 'Object' instead."
                return false
            @initialData = @_applySeparator @_formatEntity ''
        return true
        
    startNewObject : (key) ->
        if @_specifyNestPrefix key
            @_beginNewStream "Object"

    startNewArray : (key) ->
        if @_specifyNestPrefix key
            @_beginNewStream "Array"
    
        
    #Returns a new stream that is connected to the current one. Two versions in arguments: (key,newType) or (newType)
    _beginNewStream : (newType) ->
        
        #Copy options as the same as this object was created with
        options = @options
        options.type = newType
        
        if @prettyFormat
            options.tabLevel = @tabLevel + 1
        

        
        options.preData = @initialData ? ""
        options.isNested = true
        @hasNested = true
        
        
        
        newStream = new StreamStringify(options)
        newStream.pipe(@, {end: false})
        
        
        
        return newStream

    
    #Attach the condition that if the piped source ends when the current one is closed, then end this pipe (since it serves no purpose anymore)
    pipe : (dest,options) ->
        super(dest,options)
        
        stop = (data) =>
            @stop()
        
        
        dest.on('end',stop)


        #Make sure we stop if there are new things starting
        dest.once('pipe',stop)
        dest.once('beforeEnd',stop)
        
        
        
        #Inspired by the original stream.js: https://github.com/joyent/node/blob/master/lib/stream.js
        cleanup = =>
            dest.removeListener('end',stop)
            dest.removeListener('pipe',stop)
            dest.removeListener('beforeEnd',stop)
            @removeListener('end',cleanup)
            @removeListener('close',cleanup)
            dest.removeListener('close',cleanup)
        
        @on('end',cleanup)
        @on('close',cleanup)
        dest.on('close',cleanup)
        
        
        
        return @
        

    drain : ->
        if not @paused and @buffer
            data = @buffer
            @buffer = ''
            if data == null
                return @emit('end')
            else
                @emit('data',data)
        return @
        
    
    queue : (data) ->
        if @_ended or @stopped then return @
        if data == null then @_ended=true
        
        
        @buffer+=data
        @drain()
        return @
        
    #Identical to queue
    push : (data) -> @queue(data)

    #Closes the stream, it will end when not needed anymore by a pipe. "Graceful" ending
    stop: ->
        if not @stopped
            @emit('beforeEnd')
            #If it is totally empty and we shouldn't print it
            if not (not @outputEmpty and @first)
                if @first
                    @queue @preData
                else if @prettyFormat
                    @cl = "\n#{@prevTabCharacter}#{@cl}"
                @queue @cl
                
                #Empty the last part
                @resume()
        @stopped = true
        
        #If there's nothing nested, we can end here
        #if not @hasNested
        @end()
        return @
        
    end : (data) ->
        if @ended then return
        @ended = true
        @emit('end')
        if data?
            @write(data)
        if not @readable and @autoDestroy
            @destroy()
        return @
    
    destroy : ->
        if @destroyed then return
        @destroyed = true
        @ended = true
        @buffer = ''
        @writable = @readable = false
        StreamStringify.noStreams -= 1
        @emit('close')
        
        return @
    
    pause : ->
        if @paused then return
        @paused = true
        return @
    
    resume : ->
        if @paused
            @paused = false
            @emit('resume')
            @emit('drain')
        @drain()
        return @

   
        
        
    
    
module.exports = ((opts) -> new StreamStringify(opts))
module.exports.activeStreams = StreamStringify.noStreams


