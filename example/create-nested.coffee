sstringify = require './../src/stream-stringify'


jsonString = ''
root = sstringify().on('data', (data) -> jsonString += data)
root.writeKeyValue "firstKey", "firstValue"
nestedObject = root.startNewObject "nestedObject"
innerArray = nestedObject.startNewArray "innerArray"
innerArray.writeElement 1
innerArray.writeElement [2,3]
nestedObject.writeKeyValue "anotherInnerKey", "anotherInnerValue"
root.stop()

console.log JSON.stringify({
  "firstKey": "firstValue",
  "nestedObject": {
    "innerArray": [
      1,
      [
        2,
        3
      ]
    ],
    "anotherInnerKey": "anotherInnerValue"
  }
},null,2)

console.log jsonString
